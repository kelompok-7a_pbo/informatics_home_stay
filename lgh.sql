-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2018 at 04:57 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lgh`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` varchar(15) NOT NULL,
  `pass` varchar(16) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hp` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `pass`, `nama`, `email`, `hp`) VALUES
('12345', '54321', 'Muhlis', 'muhlis@gmail.com', '082146750962'),
('crs', 'crs123', 'Chaerus Sulton', 'crs@gmail.com', '081256478932'),
('lgh1', '123abc', 'L G Holy', 'holy@gmail.com', '2147483647');

-- --------------------------------------------------------

--
-- Table structure for table `boking`
--

CREATE TABLE `boking` (
  `id_boking` int(7) NOT NULL,
  `jenis` varchar(12) NOT NULL,
  `tgl_awal` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `lama` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `boking`
--

INSERT INTO `boking` (`id_boking`, `jenis`, `tgl_awal`, `tgl_akhir`, `lama`) VALUES
(1, 'Traveloka', '2018-06-04', '2018-06-09', 5),
(2, 'Booking.com', '2018-06-06', '2018-06-07', 1),
(3, 'On The Spot', '2018-06-05', '2018-06-09', 4),
(4, 'Traveloka', '2018-06-04', '2018-06-20', 16),
(5, 'Booking.com', '2018-06-09', '2018-06-14', 5),
(6, 'On The Spot', '2018-06-04', '2018-05-04', -30),
(7, 'On The Spot', '2018-06-04', '2018-06-23', 19),
(8, 'Traveloka', '2018-06-04', '2018-06-13', 9),
(9, 'Booking.com', '2018-06-07', '2018-06-12', 5),
(10, 'On The Spot', '2018-06-06', '2018-06-10', 4),
(11, 'On The Spot', '2018-06-06', '2018-06-13', 7),
(12, 'Traveloka', '2018-06-07', '2018-06-14', 7),
(13, 'On The Spot', '2018-06-07', '2018-06-07', 0),
(14, 'Booking.com', '2018-06-07', '2018-06-07', 0),
(15, 'On The Spot', '2018-06-07', '2018-06-07', 0),
(16, 'On The Spot', '2018-06-07', '2018-06-07', 0),
(17, 'Traveloka', '2018-06-09', '2018-06-15', 6),
(18, 'Traveloka', '2018-07-05', '2018-07-13', 8),
(19, 'Booking.com', '2018-07-05', '2018-07-21', 16);

-- --------------------------------------------------------

--
-- Table structure for table `identitas`
--

CREATE TABLE `identitas` (
  `id_tamu` varchar(16) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `kwn` varchar(30) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `identitas`
--

INSERT INTO `identitas` (`id_tamu`, `nama`, `kwn`, `no_hp`, `email`) VALUES
('1235689', 'Muhlis Fathurrahman', 'Indonesia', '089686873739', 'muhlisfathurrahman@gmail.com'),
('21231254688', 'Ririn Perwita Sari Puspita', 'Korea Selatan', '081236597842', 'ririn@gmail.com'),
('5272050101910001', 'Evan Dimas Darmono', 'Indonesia', '083129654987', 'evanDD@gmail.com'),
('5272050207990001', 'Ahmad Mashuri', 'Indonesia', '081526393323', 'ahmadm@gmail.com'),
('5272050208970001', 'Muhajir', 'Indonesia', '087123456789', 'muhjir@gmail.com'),
('5272050909950001', 'Septian David Maulana', 'Indonesia', '085232212236', 'septiandavid@gmail.com'),
('5272051010950001', 'Febri Haryadi', 'Indonesia', '087763591753', 'febribow@gmail.com'),
('5272051208970001', 'Arifudin', 'Indonesia', '082145632598', 'fudin@gmail.com'),
('5272052808980001', 'Muhlis Fathurrahman', 'Indonesia', '089686873739', 'muhlisfathurrahman@gmail.com'),
('5272053005970001', 'Ridwan Bahri', 'Indonesia', '081725869451', 'ridwan@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE `kamar` (
  `nomor` int(2) NOT NULL,
  `harga` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`nomor`, `harga`) VALUES
(2, 300000),
(3, 240000),
(4, 200000),
(5, 270000),
(6, 220000),
(7, 270000);

-- --------------------------------------------------------

--
-- Table structure for table `nginap`
--

CREATE TABLE `nginap` (
  `id_tamu` varchar(16) NOT NULL,
  `id_boking` int(3) NOT NULL,
  `nomor` int(2) NOT NULL,
  `total_harga` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nginap`
--

INSERT INTO `nginap` (`id_tamu`, `id_boking`, `nomor`, `total_harga`) VALUES
('5272052808980001', 1, 5, 1650000),
('5272050208970001', 2, 6, 220000),
('5272050207990001', 3, 4, 720000),
('5272051208970001', 4, 6, 3520000),
('5272053005970001', 5, 4, 1300000),
('5272052912870001', 6, 6, -6000000),
('5272050208870001', 7, 2, 4180000),
('5272050208970001', 8, 4, 2340000),
('5272050101910001', 9, 5, 1350000),
('5272050909950001', 10, 3, 880000),
('5272051010950001', 11, 2, 1540000),
('1452', 12, 7, 2310000),
('asdsada', 13, 4, 0),
('asdfsfwerg', 14, 7, 0),
('', 15, 7, 0),
('tutytuyty', 16, 5, 0),
('21231254688', 17, 4, 1560000),
('1235689', 18, 5, 2640000),
('1235689', 19, 7, 4320000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boking`
--
ALTER TABLE `boking`
  ADD PRIMARY KEY (`id_boking`);

--
-- Indexes for table `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`id_tamu`);

--
-- Indexes for table `kamar`
--
ALTER TABLE `kamar`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `nginap`
--
ALTER TABLE `nginap`
  ADD UNIQUE KEY `id_boking` (`id_boking`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boking`
--
ALTER TABLE `boking`
  MODIFY `id_boking` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
