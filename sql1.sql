SELECT
     identitas.`nama`,
     boking.`jenis`,
     boking.`tgl_awal`,
     boking.`tgl_akhir`,
     boking.`lama`,
     nginap.`total_harga`
FROM
     `identitas` identitas INNER JOIN `nginap` nginap ON identitas.`id_tamu` = nginap.`id_tamu`
     INNER JOIN `boking` boking ON nginap.`id_boking` = boking.`id_boking`
WHERE
     MONTH(tgl_awal) = 5